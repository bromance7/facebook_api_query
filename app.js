const express = require('express');
const app = express();
const axios = require("axios");
const moment = require("moment");
const access_token = require("./config/keys").accessToken

app.get("/", (req, res) => {
  res.send("test test yo");
})

app.get("/daily", (req, res) => {
  const today = moment(new Date).format("YYYY-MM-DD");
  axios.get(`https://graph.facebook.com/v7.0/act_978582492499632/insights?fields=cost_per_action_type&time_range={"since":"${today}","until":"${today}"}&access_token=${access_token}`)
    .then((data) => {
      let lead = data.data.data[0].cost_per_action_type.find(ele => {
        return ele.action_type === 'lead'
      })
      res.send(lead.value)
    })
    .catch((err) => console.log(err))
})

app.get("/weekly", (req, res) => {
  const today = moment(new Date).format("YYYY-MM-DD");
  const weekAgo = moment(new Date).subtract(7, 'days').format("YYYY-MM-DD");
  axios.get(`https://graph.facebook.com/v7.0/act_978582492499632/insights?fields=cost_per_action_type&time_range={"since":"${weekAgo}","until":"${today}"}&access_token=${access_token}`)
    .then((data) => {
      let lead = data.data.data[0].cost_per_action_type.find(ele => {
        return ele.action_type === 'lead'
      })
      res.send(lead.value)
    })
    .catch((err) => console.log(err))
})

app.get("/monthly", (req, res) => {
  const today = moment(new Date).format("YYYY-MM-DD");
  const monthAgo = moment(new Date).subtract(1, 'months').format("YYYY-MM-DD");
  axios.get(`https://graph.facebook.com/v7.0/act_978582492499632/insights?fields=cost_per_action_type&time_range={"since":"${monthAgo}","until":"${today}"}&access_token=${access_token}`)
    .then((data) => {
      let lead = data.data.data[0].cost_per_action_type.find(ele => {
        return ele.action_type === 'lead'
      })
      res.send(lead.value)
    })
    .catch((err) => console.log(err))
})


app.listen(4000, () => console.log("listening to server, port 4000"))